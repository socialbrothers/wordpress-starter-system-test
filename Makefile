include ./../.env
.DEFAULT_GOAL := help

# Variables used throughout the Makefile
USER:=`whoami`
TIMESTAMP:=`date +%F`

ORANGE=\033[1m\033[38;5;208m
GRAY=\033[38;5;242m
NC=\033[0m

# Make if not set silent by default
ifndef VERBOSE
.SILENT:
endif

# Make Windows/Linux specif prefixes
ifeq ($(OS),Windows_NT)
    PREFIX_DOCKER := winpty
	PREFIX_COLOR := -e
else
endif

help:
	@make message MESSAGE="Social Brothers Functions"
	@echo "$$(grep -hE '^\S+:.*##' $(MAKEFILE_LIST) | sed -e 's/:.*##\s*/:/' -e 's/^\(.\+\):\(.*\)/\\x1b[36m\1\\x1b[m:\2/' | column -c2 -t -s :)"

confirm:
	@echo ${PREFIX_COLOR} "${GRAY}Are you sure you want to do this? [y/N]:${NC}"
	@(read -p "" sure && case "$$sure" in [yY]) true;; *) false;; esac )

message:
	@echo "";
	@echo ${PREFIX_COLOR} "${ORANGE}${MESSAGE}${NC}"

init: ## Initializing a new project
	@make message MESSAGE="Initializing a new project and with a new GIT-repository for ${GIT_REMOTE_NAME}..."
	@make confirm

	@make message MESSAGE="This will remove all the GIT history of your current repository (${GIT_REMOTE_URI}) ..."
	@make confirm

	@cd ./../ && git checkout --orphan temporary
	@cd ./../ && git add -A
	@cd ./../ && git remote rm origin
	@cd ./../ && git remote add origin ${GIT_REMOTE_URI}
	@cd ./../ && git commit -m "Initialize"
	@cd ./../ && git branch | grep -v "temporary" | xargs git branch -D
	@cd ./../ && git branch -m master
	@cd ./../ && git push -u origin master
	@cd ./../ && git checkout -b develop
	@cd ./../ && git push -u origin develop
	@cd ./../ && git checkout -b feature/setup
	@cd ./../ && git push -u origin feature/setup
	@cd ./../ && git gc --aggressive --prune=all

setup: ## Setup the local development envirnoment
	@make message MESSAGE="Downloading Node.js Modules..."
	@cd ./../ && yarn install

	@make message MESSAGE="Downloading Node.js Dependencies..."
	@yarn install

	@make plugins

	@make message MESSAGE="Activating WordPress theme..."
	@cd ./../app/public && wp theme activate socialbrothers

	@make message MESSAGE="Activating WordPress plugins..."
	@cd ./../app/public && wp plugin activate --all


plugins: ## Downloading WordPress Plugins
	@make message MESSAGE="Downloading WordPress plugins..."
	@cd ./../ && composer install --no-autoloader

pull: ## Replacing local database with live database
	@make message MESSAGE="Replacing local database with ${MIGRATEDBPRO_URL} database..."
	@make confirm

	@make message MESSAGE="This means all your local changes will be overwritten with ${MIGRATEDBPRO_URL}..."
	@make confirm

	@make message MESSAGE="Backing up current database..."
	@cd ./../app/public && wp db export ./../../data/${TIMESTAMP}-${USER}.sql --allow-root

	@make message MESSAGE="Initializing..."
	@cd ./../app/public && wp plugin activate wp-migrate-db-pro
	@cd ./../app/public && wp plugin activate wp-migrate-db-pro-media-files
	@cd ./../app/public && wp plugin activate wp-migrate-db-pro-cli
	@cd ./../app/public && wp migratedb setting update license $(MIGRATEDBPRO_LICENSE)

	@make message MESSAGE="Pulling Database..."
	@echo ${PREFIX_COLOR} "${GRAY}Do you also want to download the images? [y/N]:${NC}"
	@(read -p "" sure && case "$$sure" in [yY]) cd ./../app/public && wp migratedb pull $(MIGRATEDBPRO_URL) $(MIGRATEDBPRO_KEY) --media=remove-and-copy;; *) cd ./../app/public && wp migratedb pull $(MIGRATEDBPRO_URL) $(MIGRATEDBPRO_KEY);; esac )

push: ## Replacing live database with local database
	@make message MESSAGE="Replacing ${MIGRATEDBPRO_URL} database with local database..."
	@make confirm

	@make message MESSAGE="This means all your ${MIGRATEDBPRO_URL} changes will be overwritten with local..."
	@make confirm

	@make message MESSAGE="Darn, are you really sure? This is not common!"
	@make confirm

	@make message MESSAGE="Initializing..."
	@cd ./../app/public && wp plugin activate wp-migrate-db-pro
	@cd ./../app/public && wp plugin activate wp-migrate-db-pro-media-files
	@cd ./../app/public && wp plugin activate wp-migrate-db-pro-cli
	@cd ./../app/public && wp migratedb setting update license $(MIGRATEDBPRO_LICENSE)

	@make message MESSAGE="Pushing Database..."
	@cd ./../app/public && wp migratedb push $(MIGRATEDBPRO_URL) $(MIGRATEDBPRO_KEY)

import: ## Replacing local database and uploads with latest export
	@make message MESSAGE="Replacing local database with latest export..."
	@make confirm

	@make message MESSAGE="Backing up current database..."
	@cd ./../app/public && wp db export ./../../data/${TIMESTAMP}-${USER}.sql --allow-root

	@make message MESSAGE="Backing up current uploads..."
	@zip -r ./../data/${TIMESTAMP}-${USER}.zip ./../app/public/wp-content/uploads
	@rm -rfv ./../app/public/wp-content/uploads/*

	@make message MESSAGE="Importing latest export..."
	@cd ./../app/public && wp db import ./../../data/latest.sql --allow-root

	@make message MESSAGE="Importing latest uploads..."
	@unzip ./../data/latest.zip -d ./../app/public/wp-content/uploads

export: ## Creating export of current database and uploads
	@make message MESSAGE="Creating export of current database..."

	@cd ./../app/public && wp db export ./../../data/latest.sql --allow-root

	@make message MESSAGE="Creating export of current uploads..."
	@zip -r ./../data/latest.zip ./../app/public/wp-content/uploads

backup: ## Creating backup of current database and uploads
	@make message MESSAGE="Backing up current database..."
	@cd ./../app/public && wp db export ./../../data/${TIMESTAMP}-${USER}.sql --allow-root

	@make message MESSAGE="Backing up current uploads..."
	@zip -r ./../data/${TIMESTAMP}-${USER}.zip ./../app/public/wp-content/uploads
